from math import exp


def f(x):
    """
    activation function (sigmoid function)
    :param x: input value of activate function
    :return: activate result
    """
    return 1 / (1 + exp(-1 * x))


def f_derivative(x):
    """
    derivative of activation function
    :param x: input value of derivative 
    :return: derivative result 
    """
    return 1 / (2 + exp(-1 * x) + exp(x))


class NeuralNetwork:
    input_layer = [1, 1]
    hidden_sum = [0] * 3
    hidden_result = [0] * 3
    weights_IH = [[0.8, 0.4, 0.3], [0.2, 0.9, 0.5]]
    weights_HO = [0.3, 0.5, 0.9]
    output_sum = 0
    output_result = 0
    target = 0
    output_sum_margin_error = 0
    delta_output_result = 0
    delta_weight_HO = [0] * 3
    delta_hidden_sum = [0] * 3
    delta_weight_IH = [[0] * 3, [0] * 3]

    def __init__(self):
        pass

    def train(self):
        """
        training function
        :return: 
        """
        for i in range(0, 3):
            for j in range(0, 2):
                self.hidden_sum[i] += self.input_layer[j] * self.weights_IH[j][i]
            self.hidden_result[i] = f(self.hidden_sum[i])
            self.output_sum += self.hidden_result[i] * self.weights_HO[i]
        self.output_result = f(self.output_sum)
        print("output sum: ", self.output_sum)
        print("output result: ", self.output_result)

    def back_propagation(self):
        """
        back propagation function
        :return: 
        """
        self.output_sum_margin_error = self.target - self.output_result
        self.delta_output_result = f_derivative(self.output_sum) * self.output_sum_margin_error
        print("delta output result: ", self.delta_output_result)
        for i in range(0, 3):
            self.delta_weight_HO[i] = self.delta_output_result / self.hidden_result[i]
            self.delta_hidden_sum[i] = self.delta_output_result / self.weights_HO[i] * f_derivative(self.hidden_sum[i])
            for j in range(0, 2):
                self.delta_weight_IH[j][i] = self.delta_hidden_sum[i] / self.input_layer[j]
        print("delta weight_HO: ", self.delta_weight_HO)
        print("delta hidden sum: ", self.delta_hidden_sum)
        print("delta weight_IH: ", self.delta_weight_IH)

    def update_weights(self):
        for i in range(0, 3):
            self.weights_HO[i] += self.delta_weight_HO[i]
            for j in range(0, 2):
                self.weights_IH[j][i] += self.delta_weight_IH[j][i]
        print("new weight_IH: ", self.weights_IH)
        print("new weight_HO: ", self.weights_HO)


if __name__ == '__main__':
    print("=============== Start training ===============\n")
    NN = NeuralNetwork()
    NN.train()
    NN.back_propagation()
    NN.update_weights()
